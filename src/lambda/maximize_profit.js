module.exports = (stockPricesYesterday) => {
    const stockPrices = stockPricesYesterday && Array.isArray(stockPricesYesterday) ?
        stockPricesYesterday : [],
        len = stockPrices.length,
        isValidPrice = (p) => { return "number" === typeof p && 0 <= p; };
    return stockPrices.reduce(([profit, buy, sell], price, index) => {
        if (isValidPrice(price) && index < len - 1) {
            const rightSlice = stockPrices.slice(index + 1),
                maxPrice = Math.max(...rightSlice.filter(isValidPrice)),
                delta = maxPrice - price;
            if (delta > profit) {
                [profit, buy, sell] = [
                    +delta.toFixed(2),
                    index,
                    index + 1 + rightSlice.indexOf(maxPrice)
                ];
            }
        }
        return [profit, buy, sell];
    }, [-1, 0, 0]);
};
