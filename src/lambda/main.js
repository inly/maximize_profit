const moment = require("moment");
const maximizeProfit = require("./maximize_profit.js");
const getTime = (minutes) => {
    const START_TIME = moment(`${moment().format("YYYY-MM-DD")} 10:00:00`);
    return START_TIME.add(minutes, "minutes").format("YYYY-MM-DD HH:mm");
};
module.exports.maximize = async (event) => {
    const [profit, buy, sell] = maximizeProfit(event.stockPricesYesterday),
        [buyAt, sellAt] = profit >= 0 ? [getTime(buy), getTime(sell)] : ["", ""];
    return { profit, buyAt, sellAt };
};
