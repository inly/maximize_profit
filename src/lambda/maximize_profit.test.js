const maximizeProfit = require("./maximize_profit.js");
describe("Test Get Max Profit", () => {
    const scenarios = [{
        description: "When yesterday's stock prices are not provided",
        stockPricesYesterday: null,
        expectedMaxProfit: -1,
        expectedBuy: 0,
        expectedSell: 0
    }, {
        description: "When yesterday's stock prices are incorrect",
        stockPricesYesterday: "a",
        expectedMaxProfit: -1,
        expectedBuy: 0,
        expectedSell: 0
    }, {
        description: "When yesterday's stock prices are not as expected",
        stockPricesYesterday: 1,
        expectedMaxProfit: -1,
        expectedBuy: 0,
        expectedSell: 0
    }, {
        description: "When yesterday's stock prices only have 1 value",
        stockPricesYesterday: [1],
        expectedMaxProfit: -1,
        expectedBuy: 0,
        expectedSell: 0
    }, {
        description: "When yesterday's stock prices only have 2 values and are in decending order",
        stockPricesYesterday: [9, 8],
        expectedMaxProfit: -1,
        expectedBuy: 0,
        expectedSell: 0
    }, {
        description: "When yesterday's stock prices have more values but are in decending order",
        stockPricesYesterday: [9, 8, 7, 6, 5, 4, 3, 2, 1],
        expectedMaxProfit: -1,
        expectedBuy: 0,
        expectedSell: 0
    }, {
        description: "When yesterday's stock prices have only 2 values and are in ascending order",
        stockPricesYesterday: [5, 20],
        expectedMaxProfit: 15,
        expectedBuy: 0,
        expectedSell: 1
    }, {
        description: "When yesterday's stock prices have more values and are not in decending order",
        stockPricesYesterday: [10, 7, 5, 8, 11, 9],
        expectedMaxProfit: 6,
        expectedBuy: 2,
        expectedSell: 4
    }, {
        description: "When yesterday's stock prices are reversed from above",
        stockPricesYesterday: [9, 11, 8, 5, 7, 10],
        expectedMaxProfit: 5,
        expectedBuy: 3,
        expectedSell: 5
    }, {
        description: "When yesterday's stock prices are all the same",
        stockPricesYesterday: [9, 9, 9, 9, 9, 9],
        expectedMaxProfit: 0,
        expectedBuy: 0,
        expectedSell: 1
    }, {
        description: "When yesterday's stock prices have 1 decimal",
        stockPricesYesterday: [5.2, 20.1],
        expectedMaxProfit: 14.9,
        expectedBuy: 0,
        expectedSell: 1
    }, {
        description: "When yesterday's stock prices have 2 decimals",
        stockPricesYesterday: [5.62, 20.19],
        expectedMaxProfit: 14.57,
        expectedBuy: 0,
        expectedSell: 1
    }, {
        description: "When yesterday's stock prices have more decimals, but < .5",
        stockPricesYesterday: [5.2234, 20.11123],
        expectedMaxProfit: 14.89,
        expectedBuy: 0,
        expectedSell: 1
    }, {
        description: "When yesterday's stock prices have more decimals, but >= .5",
        stockPricesYesterday: [5.2267, 20.1189],
        expectedMaxProfit: 14.89,
        expectedBuy: 0,
        expectedSell: 1
    }, {
        description: "When yesterday's stock prices have some noises",
        stockPricesYesterday: [5, "a", "b", 20, -1, -2, -3],
        expectedMaxProfit: 15,
        expectedBuy: 0,
        expectedSell: 3
    }, {
        description: "When yesterday's stock prices have duplicated values",
        stockPricesYesterday: [9, 11, 8, 5, 7, 10, 9, 11, 8, 5, 7, 10, 9, 11, 8, 5, 7, 10, 9, 11, 8, 5, 7, 10],
        expectedMaxProfit: 6,
        expectedBuy: 3,
        expectedSell: 7
    }];
    for (const item of scenarios) {
        it(`${item.description}`, async () => {
            expect(
                maximizeProfit(item.stockPricesYesterday)
            ).toEqual(
                [item.expectedMaxProfit, item.expectedBuy, item.expectedSell]
            );
        })
    }
});
