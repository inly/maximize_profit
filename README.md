### Layout
* Infra - AWS CDK IaC
    * Stack: `profit-service-[env]`.
        * Lambda: `profit-service-[env]-maximize`, no packaging needed.
        * Lambda Layer: `profit-service-layer-[env]`, added to above lambda.
        * SSM: `/profit-service/layer-[env]/latest` to store lambda layer latest version.

* Src
    * Lambda
        * Lambda hander: `main.js`.
        * Helper: `maximize_profit.js`.
        * Unit test: `maximize_profit.test.js`.
    * layer/nodejs
        * `package.json`: lambda layer.

* `build.sh`: script to build lambda layer.
* `config.json`: configs.
* `.gitlab-ci.yml`: setup gitlab CI, (invalid AWS setup, wont deploy)

### CI
* Image:
    * node:14-slim
* Steps
    * Test
        * Run unit test in container. 
    * Deploy
        * Deploy stack via AWS CDK in container.

### Lambda
* Expected input
```json
{
    "stockPricesYesterday":[
        9, 11, 8, 5, 7, 10
    ]
}
```
* Expected output
```json
{ 
    "profit": 5, 
    "buyAt": "2021-09-03 10:03", 
    "sellAt": "2021-09-03 10:05" 
}
```
