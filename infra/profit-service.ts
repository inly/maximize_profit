import * as cdk from "@aws-cdk/core";
import * as ssm from "@aws-cdk/aws-ssm";
import * as logs from "@aws-cdk/aws-logs";
import * as lambda from "@aws-cdk/aws-lambda";

interface ProfitServiceStackProps extends cdk.StackProps {
    id: string;
    awsEnv: string;
}

export class ProfitServiceStack extends cdk.Stack {
    constructor(scope: cdk.Construct, id: string, props: ProfitServiceStackProps) {
        super(scope, id, props);
        const prefix = id + "-",
            lambdaLayer = new lambda.LayerVersion(this, "profit-service-layer-" + props.awsEnv, {
                code: lambda.Code.fromAsset("./src/layer"),
                compatibleRuntimes: [lambda.Runtime.NODEJS_14_X],
                layerVersionName: "profit-service-layer-" + props.awsEnv
            });
        new ssm.StringParameter(this, prefix + "layer", {
            parameterName: `/profit-service/layer-${props.awsEnv}/latest`,
            stringValue: lambdaLayer.layerVersionArn
        });
        const lambdaMaximizeProfit = new lambda.Function(this, `${prefix}maximize`, {
            functionName: `${prefix}maximize`,
            runtime: lambda.Runtime.NODEJS_14_X,
            code: lambda.Code.fromAsset("./src/lambda"),
            handler: "main.maximize",
            timeout: cdk.Duration.minutes(3),
            logRetention: logs.RetentionDays.TWO_WEEKS
        });
        lambdaMaximizeProfit.addLayers(lambdaLayer);
    }
}
