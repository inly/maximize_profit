import * as cdk from "@aws-cdk/core";
import config from "../config.json";
import { ProfitServiceStack } from "./profit-service";

const app = new cdk.App();
new ProfitServiceStack(app, config.dev.id, {
    env: {
        region: config.dev.region,
        account: config.dev.account
    },
    awsEnv: "dev",
    id: config.dev.id,
});
